const TelegramBot = require('node-telegram-bot-api');
// const bot = new TelegramBot('1421211856:AAF-IJGVaYMc9PFbzLC5ALg2x1J141ARMJg');
// const chatId = '-1001200411182';

class TgBotSender {
    constructor(token = '1421211856:AAF-IJGVaYMc9PFbzLC5ALg2x1J141ARMJg') {
        this.bot = new TelegramBot(token);
    }
    send(chatId = '-1001200411182', options) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.bot.sendMessage(chatId, JSON.stringify(options), { parse_mode: 'html' });
            } catch (e) {
                reject(e);
            }
        });
    }
}

module.exports = TgBotSender;