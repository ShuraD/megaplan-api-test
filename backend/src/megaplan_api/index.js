const Deal = require('./mediators/Deal');
const Program = require('./mediators/Program');
const FilterList = require('./filters/List');
const DealRequestBodyBuilder = require('./requestBodyBuilders/Deal');

module.exports.DealManager = function(client) {
    return new Deal(client);
};

module.exports.ProgramManager = function(client) {
    return new Program(client);
};

module.exports.FilterList = function(pageAfter = null, pageBefore = null, pageWith = null, limit = null) {
    return new FilterList(pageAfter, pageBefore, pageWith, limit);
};

module.exports.DealRequestBodyBuilder = new DealRequestBodyBuilder();
