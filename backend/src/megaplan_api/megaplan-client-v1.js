const axios = require('axios');
const crypto = require('crypto');
const qs = require('qs');

class Client {
    constructor(host, user, password) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.accessId = null;
        this.secretKey = null;
    }
    auth() {
        const self = this;
        return new Promise(async (resolve, reject) => {
            const hash = crypto.createHash('md5').update(self.password).digest('hex');
            try {
                const response = await axios({
                    method: 'post',
                    url: self.host + '/BumsCommonApiV01/User/authorize.api',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    data: qs.stringify({
                        Login: self.user,
                        Password: self.password,
                        OneTimeKey: '',
                    })
                });
                resolve(response);
            } catch (e) {
                reject(e);
            }
        });
    }
}

const client = new Client('https://control-mos.ru', 'admin', 'Admin1Admin');
try {
    client.auth().then().catch(e => console.log(e.response.data));
} catch(e) {
    // console.log(e);
}

// module.exports = Client;