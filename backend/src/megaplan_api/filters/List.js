class List {
    constructor(pageAfter = null, pageBefore = null, pageWith = null, limit = null) {
        this.pageAfter = pageAfter;
        this.pageBefore = pageBefore;
        this.pageWith = pageWith;
        this.limit = limit;
    }
}

module.exports = List;