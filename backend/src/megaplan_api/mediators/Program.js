const AbstractMediator = require('./AbstractMediator');

class Program extends AbstractMediator {
    constructor(client) {
        super(client);
    }
    getAll() {
        const url = '/api/v3/program/';
        return this.get(url);

    }
    getInfo(id) {
        const url = '/api/v3/program/' + id;
        return this.get(url);
    }
}

module.exports = Program;