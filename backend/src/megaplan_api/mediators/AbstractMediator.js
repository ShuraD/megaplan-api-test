class AbstractMediator {
    constructor(client) {
        this.client = client;
    }
    get(url) {
        return this.client.get(url);
    }
    post(url, data) {
        return this.client.post(url, data);
    }
    delete(url) {
        return this.client.delete(url);
    }
}

module.exports = AbstractMediator;