'use strict';
module.exports = (sequelize, DataTypes) => {
    const Requests = sequelize.define('Requests', {
        name: DataTypes.STRING,
        path: DataTypes.STRING,
        http: DataTypes.STRING,
        avg: DataTypes.FLOAT,
        max: DataTypes.INTEGER,
        min: DataTypes.INTEGER,
        qty: DataTypes.INTEGER
    }, {});
    Requests.associate = function (models) {
        // associations can be defined here
    };
    return Requests;
};