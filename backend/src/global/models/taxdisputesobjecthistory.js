'use strict';
module.exports = (sequelize, DataTypes) => {
    const TaxDisputesObjectHistory = sequelize.define('TaxDisputesObjectsHistory', {
        crud: DataTypes.STRING,
        table: DataTypes.STRING,
        rowId: DataTypes.INTEGER,
        from: DataTypes.JSON,
        to: DataTypes.JSON,
        userId: DataTypes.INTEGER,
    }, {});
    TaxDisputesObjectHistory.associate = function (models) {
        // associations can be defined here
    };
    return TaxDisputesObjectHistory;
};