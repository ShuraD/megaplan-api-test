'use strict';
module.exports = (sequelize, DataTypes) => {
    const TaxDisputesObject = sequelize.define('TaxDisputesObject', {
        dealId: DataTypes.INTEGER,
        // Требование (суть)
        requirement: DataTypes.STRING,
        // Кадастровый номер
        cadastralNumber: DataTypes.STRING,
        // Кадастровый номер родительского объекта
        cadastralNumberParent: DataTypes.STRING,
        // Полный адрес объекта
        fullAddress: DataTypes.TEXT,
        // Площадь
        square: DataTypes.FLOAT,
        // Назначение объекта
        objectPurpose: DataTypes.STRING,
        // Тип объекта
        objectType: DataTypes.STRING,
        // Кадастровая стоимость
        cadastralValue: DataTypes.FLOAT,
        // Налоговый потенциал
        taxPotential: DataTypes.FLOAT,
        // Включение в перечень
        listInclusion: DataTypes.STRING,
        // Пункт перечня
        listItem: DataTypes.STRING,
        // Год перечня
        listYear: DataTypes.INTEGER,
        // Критерий включения
        criteriaInclusion: DataTypes.STRING,
        // Категория спора
        disputeCategory: DataTypes.TEXT,
        // Результат в первой инстанции
        firstInstanceResult: DataTypes.TEXT,
        // Сведения об обжаловании
        appealInformation: DataTypes.TEXT,
        // Результат в апелляционной/кассационной инстанции
        appealCassationResult: DataTypes.STRING,
        // Кадастровый номер ЗУ
        cadastralNumberZU: DataTypes.TEXT,
        // ВРИ ЗУ
        vriZU: DataTypes.TEXT,
        // Номер акта
        actNumber: DataTypes.STRING,
        // Дата акта
        actDate: DataTypes.DATE,
        // Правообладатель
        copyrightHolder: DataTypes.STRING,
        // Решение в кассационной инстанции
        cassationResult: DataTypes.STRING,
        // Ссылка на объект
        objectLink: DataTypes.STRING,
        isDeleted: DataTypes.BOOLEAN,
    }, {});
    TaxDisputesObject.associate = function (models) {
        // associations can be defined here
    };
    return TaxDisputesObject;
};