'use strict';
module.exports = (sequelize, DataTypes) => {
    const NioObjectsHistory = sequelize.define('NioObjectsHistory', {
        crud: DataTypes.STRING,
        table: DataTypes.STRING,
        rowId: DataTypes.INTEGER,
        from: DataTypes.JSON,
        to: DataTypes.JSON,
        userId: DataTypes.INTEGER,
    }, {});
    NioObjectsHistory.associate = function (models) {
        // associations can be defined here
    };
    return NioObjectsHistory;
};