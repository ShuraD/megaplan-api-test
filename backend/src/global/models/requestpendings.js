'use strict';
module.exports = (sequelize, DataTypes) => {
    const RequestPendings = sequelize.define('RequestPendings', {
        requestTypeId: DataTypes.INTEGER,
        time: DataTypes.INTEGER
    }, {});
    RequestPendings.associate = function (models) {
        // associations can be defined here
    };
    return RequestPendings;
};