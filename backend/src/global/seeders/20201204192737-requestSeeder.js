'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('People', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */
        return queryInterface.bulkInsert('Requests', [{
            name: 'Запрос к витринам "УНИО", "ЕГРН"',
            path: 'tax_disputes/data_mart',
            http: 'get',
            createdAt: new Date(),
            updatedAt: new Date()
        }, {
            name: 'Запрос к витринам "УНИО", "ЕГРН"',
            path: 'nio/data_mart',
            http: 'get',
            createdAt: new Date(),
            updatedAt: new Date()
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('People', null, {});
        */
    }
};
