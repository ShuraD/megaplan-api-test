const winston = require('winston');
const randomstring = require('randomstring');

const customLoggingFormat = winston.format.printf(({level, timestamp, message}) => {
    return `${level} - ${(new Date(timestamp)).toLocaleString('ru-RU', {
        hour12: false,
        timeZone: 'Europe/Moscow'
    })} - ${message}`;
});

let WinstonLogWrapper = function () {
    this.pool = {};

    this.getLogger = function (logStage) {
        let logger = this.pool[logStage];

        if (typeof logger === 'undefined') {
            logger = winston.createLogger({
                format: winston.format.combine(
                    winston.format.timestamp(),
                    customLoggingFormat
                )
            });

            // if (process.env.NODE_ENV !== 'production') {
            if (logStage === 'all') {
                logger.add(new winston.transports.Console());
            }

            logger.add(new winston.transports.File({
                filename: `storage/logs/${logStage}.log`
            }));

            this.pool[`${logStage}`] = logger;
        }
        return logger;
    };

    this.log = function (stages, level, message) {
        let logger;
        for (let i = 0, counter = stages.length; i < counter; i++) {
            logger = this.getLogger(stages[i]);
            logger.log({
                'level': level,
                'message': message
            });
        }
    };

    // this.logHash = function () {
    //
    //     const hash = this.randomLogHash();
    //
    //     const logger = winston.createLogger({
    //         format: winston.format.combine(
    //             winston.format.timestamp(),
    //             customLoggingFormat
    //         )
    //     });
    //
    //     return
    // };

    this.randomLogHash = function (length = 11) {
        return randomstring.generate(length);
    };

};

module.exports = new WinstonLogWrapper;