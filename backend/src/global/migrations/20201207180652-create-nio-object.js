'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('NioObjects', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            dealId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            // Требование (суть)
            requirement: {
                type: Sequelize.STRING,
                // allowNull: false,
                // defaultValue: false,
                // type: Sequelize.DATE
            },
            // Кадастровый номер
            cadastralNumber: {
                type: Sequelize.STRING
            },
            // Кадастровый номер родительского объекта
            cadastralNumberParent: {
                type: Sequelize.STRING
            },
            // Полный адрес объекта
            fullAddress: {
                type: Sequelize.TEXT
            },
            // Площадь
            square: {
                type: Sequelize.FLOAT
            },
            // Назначение объекта
            objectPurpose: {
                type: Sequelize.STRING
            },
            // Тип объекта
            objectType: {
                type: Sequelize.STRING
            },
            // Кадастровая стоимость
            cadastralValue: {
                type: Sequelize.FLOAT
            },
            // Налоговый потенциал
            taxPotential: {
                type: Sequelize.FLOAT
            },
            // Включение в перечень
            listInclusion: {
                type: Sequelize.STRING
            },
            // Пункт перечня
            listItem: {
                type: Sequelize.STRING
            },
            // Год перечня
            listYear: {
                type: Sequelize.INTEGER
            },
            // Критерий включения
            criteriaInclusion: {
                type: Sequelize.STRING
            },
            // Категория спора
            disputeCategory: {
                type: Sequelize.TEXT
            },
            // Результат в первой инстанции
            firstInstanceResult: {
                type: Sequelize.TEXT
            },
            // Сведения об обжаловании
            appealInformation: {
                type: Sequelize.TEXT
            },
            // Результат в апелляционной/кассационной инстанции
            appealCassationResult: {
                type: Sequelize.STRING
            },
            // Кадастровый номер ЗУ
            cadastralNumberZU: {
                type: Sequelize.TEXT
            },
            // ВРИ ЗУ
            vriZU: {
                type: Sequelize.TEXT
            },
            // Номер акта
            actNumber: {
                type: Sequelize.STRING
            },
            // Дата акта
            actDate: {
                type: Sequelize.DATE
            },
            // Правообладатель
            copyrightHolder: {
                type: Sequelize.STRING
            },
            isDeleted: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('NioObjects');
    }
};