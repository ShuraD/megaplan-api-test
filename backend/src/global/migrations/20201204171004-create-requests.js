'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Requests', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING
            },
            path: {
                type: Sequelize.STRING
            },
            http: {
                type: Sequelize.STRING
            },
            avg: {
                type: Sequelize.FLOAT,
                defaultValue: 0
            },
            max: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            min: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            qty: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Requests');
    }
};