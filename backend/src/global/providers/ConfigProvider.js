const fs = require('fs');
const path = require('path');
const YAML = require('yaml');

const FILE_ENCODE = 'utf8';
const CONFIG_DEV = 'config.backend.dev.yaml';
const CONFIG_PROD = 'config.backend.prod.yaml';

class ConfigProvider {
    constructor() {
        // TODO: define production config based on process.NODE_ENV === 'production'
        const devConfig = fs.readFileSync(path.resolve(__dirname, '../../../../', CONFIG_DEV), FILE_ENCODE);
        this.config = YAML.parse(devConfig);
        if (fs.existsSync(path.resolve('../', CONFIG_PROD))) {
            const prodConfig = fs.readFileSync(path.resolve('../', CONFIG_PROD), FILE_ENCODE);
            this.config = Object.assign({}, this.config, YAML.parse(prodConfig));
        }
    }

    get(path) {
        const mode = this.config.mode;
        const keys = path.split('/');
        let tree = this.config;
        let result;
        for (let i = 0, counter = keys.length; i < counter; i++) {
            result = tree[keys[i]];
            if (typeof result === 'undefined') {
                result = tree[mode][keys[i]];
                if (typeof result === 'undefined') {
                    return null;
                }
            }
            tree = result;
        }
        return result;
    }
}

module.exports = new ConfigProvider();