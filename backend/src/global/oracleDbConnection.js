const oracledb = require('oracledb');
const Logger = require('./winston-log-wrapper');

let dbConnector = function (dbConfig) {
    this.config = dbConfig;
    this.getConnection = function () {
        let self = this;
        let millisecondsInHour = 1000 * 60 * 60;
        return new Promise(async (resolve) => {
            // let connection;
            // disable restoring connection!!!
            // for (let attempt = 1; ; attempt++) {
            //     let timeout = 5000 * attempt * attempt;
            //     if (timeout > millisecondsInHour) {
            //         timeout = millisecondsInHour;
            //     }
                try {
                    // connection = await setConnection(attempt, timeout);
                    resolve(await oracledb.getConnection(self.config));
                    // break;
                } catch (e) {
                    // __log(e.stack);
                    // __log(`FAILED TO SET DB CONNECTION, TRY TO RESTORE IT, ATTEMPT #${attempt}, TIMEOUT ${timeout / 1000}s`);
                }
            // }
            // resolve(connection);
        });

        // function setConnection(attempt, timeout) {
        //     return new Promise(async (resolve, reject) => {
        //         let connection, timer;
        //         timer = setTimeout(async () => {
        //             try {
        //                 connection = await oracledb.getConnection(self.config);
        //                 clearTimeout(timer);
        //                 resolve(connection);
        //             } catch (e) {
        //                 reject(e);
        //             }
        //         }, timeout);
        //     });
        // }
    }
    this.createTable = function (tableName, columnNames, columnTypes) {
        return new Promise(async (resolve, reject) => {
            let connection = await this.getConnection();
            let sql;

            sql = createTablePattern(tableName, columnNames, columnTypes);
            try {
                await connection.execute(sql);
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }

            function createTablePattern(tableName, tableColumns, columnsDescription) {
                let columnData = [];
                for (let i = 0, counter = tableColumns.length; i < counter; i++) {
                    columnData.push(`"${tableColumns[i]}" ${columnsDescription[i].join(' ')}`);
                }
                return `CREATE TABLE ${tableName} ( ${columnData.join(',')} )`;
            }

            resolve();
        });
    };
    this.dropTable = function (tableName) {
        return new Promise(async (resolve, reject) => {
            let sql;
            try {
                let connection = await this.getConnection();
                sql = `DROP TABLE ${tableName}`;
                await connection.execute(sql);
                resolve();
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }
        });
    };
    this.truncateTable = function (tableName) {
        return new Promise(async (resolve, reject) => {
            let sql;
            try {
                let connection = await this.getConnection();
                sql = `TRUNCATE TABLE ${tableName}`;
                await connection.execute(sql);
                resolve();
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }
        });
    };
    this.insertMany = function (query, binds, bunchQty) {
        return new Promise(async (resolve, reject) => {
            try {
                let result;
                let portion = [];
                let connection = await this.getConnection();
                let counter = Math.ceil(binds.length / bunchQty);

                for (let i = 0; i < counter; i++) {
                    portion = binds.slice(i * bunchQty, (i + 1) * bunchQty);
                    result = await connection.executeMany(query, portion);
                }

                await connection.commit();
                resolve();
            } catch (e) {
                __log(`FAILED TO EXECUTE MANY ${e.stack}, QUERY WAS ${query}, DATA WERE ${binds}`, 'error');
                reject(e);
            }
        });
    };
    this.insert = function (sql) {
        return new Promise(async (resolve, reject) => {
            try {
                let result;
                let connection = await this.getConnection();

                result = await connection.execute(sql);
                await connection.commit();
                resolve(result);
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }
        });
    };
    this.selectByColumnValue = function (table, column = null, value = null, requestedColumns = []) {
        return new Promise(async (resolve, reject) => {
            let sql;
            try {
                let result;
                let connection = await this.getConnection();

                if (requestedColumns.length) {
                    sql = `SELECT ${requestedColumns.join(',')} FROM ${table} T`;
                } else {
                    sql = `SELECT T.* FROM ${table} T`;
                }
                if (column && value) {
                    sql += ` WHERE T.${column} = '${value}'`;
                }
                result = await connection.execute(sql);
                await connection.commit();
                resolve(result);
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }
        });
    };
    this.selectCopyrightHolderName = function (cadNum) {
        return new Promise(async (resolve, reject) => {
            let sql;
            try {
                const connection = await this.getConnection();
                sql = `
                    SELECT LISTAGG(T.name, '; ')
                        WITHIN GROUP (ORDER BY T.name) AS Names
                    FROM av_egrp_20200707@jobdbp_link T
                    WHERE T.num_cadnum = '${cadNum}' and T.REG_CLOSE_REGNUM is null
                `;
                const result = await connection.execute(sql);
                await connection.commit();
                resolve(result);
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }
        });
    };
    this.checkIfTableExists = function (tableName, owner = 'READ_USER') {
        return new Promise(async (resolve, reject) => {
            let sql;
            try {
                let result;
                let connection = await this.getConnection();

                sql = `
                    SELECT CASE WHEN (
                        SELECT COUNT(*)
                        FROM sys.dba_tables
                        WHERE OWNER = UPPER('${owner}')
                            AND TABLE_NAME = UPPER('${tableName}')
                    ) > 0
                    THEN 1
                    ELSE 0
                    END as res
                    FROM dual
                    `;
                result = await connection.execute(sql);
                await connection.commit();
                resolve(result);
            } catch (e) {
                __log(`FAILED TO EXECUTE QUERY ${e.stack}, QUERY WAS ${sql}`, 'error');
                reject(e);
            }
        });
    };
    this.getInn = function(inn) {
        return new Promise(async (resolve, reject) => {
            let sql;
             try {
                 let result;
                 let connection = await this.getConnection();

                 sql = `
                    SELECT * FROM f_getinfoul(${inn})
                    `;
                 result = await connection.execute(sql);
                 await connection.commit();
                 resolve(result);
             } catch (e) {
                 reject(e);
             }
        });
    };
};

function __log(message, level = 'info') {
    Logger.log(['all'], level, message);
}

module.exports = dbConnector;
