const express = require('express');
const router = express.Router();
const db = require('../src/global/models/index');
const Logger = require('../src/global/winston-log-wrapper');
const TelegramBot = require('../src/telegram_bot/index');

const bot = new TelegramBot();

router.get('/', function(req, res,next) {
    (async function() {
        const path = req.query.path;
        const http = req.query.http;
        // TODO: validate data
        if (path && http) {
            const requestModel = db['Requests'];
            try {
                const request = await requestModel.findOne({ where: { path: path, http: http }});
                if (request) {
                    res.status(200).send({
                        status: 'ok',
                        data: request
                    });
                } else {
                    res.status(500).send({
                        status: 'error',
                        message: 'Запись отсутствует в БД'
                    });
                    __log(JSON.stringify({
                        message: 'Запись отсутствует в БД',
                        url: req.url,
                        httpMethod: 'get',
                        data: req.query
                    }));
                    await bot.send(...Array(1), {
                        message: 'Запись отсутствует в БД',
                        level: 'error',
                        module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                        url: req.url,
                        httpMethod: 'get',
                        data: req.query
                    });
                }
            } catch (e) {
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query,
                    stack: e.stack
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query,
                });
            }
        } else {
            res.status(500).send({
                status: 'error',
                message: 'Предоставлены некорректные данные'
            });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            });
        }
    })();
});

router.post('/pending', function(req, res, next) {
    (async function() {
        const path = req.body.path;
        const http = req.body.http;
        const pending = req.body.pending;
        // TODO: validate fields
        if (path && http && pending) {
            const requestModel = db['Requests'];
            try {
                const request = await requestModel.findOne({ where: { path: path, http: http }});
                if (request) {
                    const requestPendingsModel = db['RequestPendings'];
                    await requestPendingsModel.create({
                        requestTypeId: request.id,
                        time: pending
                    });
                    let avg = request.avg;
                    let max = request.max;
                    let min = request.min;
                    let qty = request.qty;
                    if (pending > max) {
                        request.max = pending;
                    }
                    if (pending < min) {
                        request.min = pending;
                    }
                    if (qty === 0) {
                        avg = pending;
                        request.max = pending;
                        request.min = pending;
                    } else {
                        avg = (avg + pending) / 2;
                    }
                    qty++;
                    request.qty = qty;
                    request.avg = avg;
                    await request.save();
                    res.status(200).send({
                        status: 'ok',
                        data: request
                    });
                } else {
                    // send 500 to prevent infinite timeout
                    __log(JSON.stringify({
                        message: 'Запись отсутствует в БД',
                        url: req.url,
                        httpMethod: 'post',
                        data: req.body
                    }));
                    await bot.send(...Array(1), JSON.stringify({
                        message: 'Запись отсутствует в БД',
                        level: 'error',
                        module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                        url: req.url,
                        httpMethod: 'post',
                        data: req.body
                    }));
                }
            } catch (e) {
                // send 500 to prevent infinite timeout
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body,
                    stack: e.stack
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body
                });
            }
        } else {
            // send 500 to prevent infinite timeout
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'post',
                data: req.body
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'post',
                data: req.body
            });
        }
    })();
});

function __log(message, level = 'error') {
    Logger.log([ 'error' ], level, message);
}

module.exports = router;