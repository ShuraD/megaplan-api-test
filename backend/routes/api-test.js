const express = require('express');
const router = express.Router();
const Client = require('megaplan-simple-client');
const { DealManager, ProgramManager } = require('api-mediators');

router.get('/program/:id', function(req, res, next) {
    (async function() {
        const id = req.params.id;
        const client = new Client('https://control-mos.ru', 'admin', 'Admin1Admin');
        const programManager = new ProgramManager(client);

        let data;
        try {
            data = await programManager.getInfo(id);
        } catch (e) {
            console.log(e);
            res.send({
                status: 'error',
                reason: 'Something went wrong',
            });
            return;
        }

        res.send(data);
    })();
});

router.get('/programs', function(req, res, next) {
    (async function() {
        const client = new Client('https://control-mos.ru', 'admin', 'Admin1Admin');
        const programManager = new ProgramManager(client);

        let data;
        try {
            data = await programManager.getAll();
        } catch (e) {
            console.log(e);
            res.send({
                status: 'error',
                reason: 'Something went wrong',
            });
            return;
        }

        res.send(data);
    })();
});

module.exports = router;