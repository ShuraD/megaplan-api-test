const express = require('express');
const router = express.Router();
const OracleDbConnector = require('../src/global/oracleDbConnection');
const ConfigProvider = require('../src/global/providers/ConfigProvider');
const Logger = require('../src/global/winston-log-wrapper');
const TelegramBot = require('../src/telegram_bot/index');

const bot = new TelegramBot();

router.get('/fast', function(req, res, next) {
    (async function() {
        const cadastralNumber = req.query.cn || null;
        if (cadastralNumber) {
            let result;
            try {
                const oracleDbConnector = new OracleDbConnector(ConfigProvider.get('oracle_cnd/connect'));
                const response = await __setCallbackTimeout(
                    oracleDbConnector.selectByColumnValue, oracleDbConnector, ['v_parent_child', 'REALTY_ID', cadastralNumber]
                );
                result = __processResponse(response);
            } catch (e) {
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                });
            }
            res.status(200).send({
                status: 'ok',
                data: result
            });
        } else {
            res.status(500).send({
                status: 'error',
                message: 'Предоставлены некорректные данные'
            });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: Налоговые споры (БП-73) / НИО (БП-74)',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            });
        }
    })();
});

router.get('/', function(req, res, next) {
    (async function() {
        const cadastralNumber = req.query.cn || null;
        const processView = req.query.pv || null;
        // TODO: check if required params exist

        const cndOracleDbOptions = ConfigProvider.get('oracle_cnd/connect');
        let result = [], response = null;
        if (cadastralNumber && processView) {
            try {
                const oracleDbConnector = new OracleDbConnector(cndOracleDbOptions);
                switch (processView) {
                    case 'parent':
                        response = await oracleDbConnector.selectByColumnValue('v_parent_child', 'REALTY_ID', cadastralNumber);
                        result = __processResponse(response);
                        let parentCadastral;
                        if (result.length > 0) {
                            parentCadastral = result[0]['OKSCADNUM'];
                            response = await oracleDbConnector.selectByColumnValue('V_OCS_INFORMATION@opndbp', 'cadastral_number', parentCadastral);
                            result = __processResponse(response);
                        }
                        break;
                    case 'unio':
                        response = await oracleDbConnector.selectByColumnValue('V_OCS_INFORMATION@opndbp', 'cadastral_number', cadastralNumber);
                        result = __processResponse(response);
                        break;
                    case 'egrn':
                        response = await oracleDbConnector.selectByColumnValue('v_object_egrn', 'REALTY_ID', cadastralNumber);
                        result = __processResponse(response);
                        break;
                }
                // TODO: close connect before send response
                res.status(200).send({
                    status: 'ok',
                    data: result
                });
            } catch (e) {
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                });
            }
        } else {
            res.status(500).send({
                status: 'error',
                message: 'Предоставлены некорректные данные'
            });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            });
        }
    })();
});

router.get('/copyright_holder_name', function(req, res, netxt) {
    (async function() {
        const cadastralNumber = req.query.cn || null;
        const cndOracleDbOptions = ConfigProvider.get('oracle_cnd/connect');
        let result = [], response = null;
        if (cadastralNumber) {
            try {
                const oracleDbConnector = new OracleDbConnector(cndOracleDbOptions);
                response = await oracleDbConnector.selectCopyrightHolderName(cadastralNumber);
                result = __processResponse(response);
                // TODO: close connect before send response
                res.status(200).send({
                    status: 'ok',
                    data: result
                });
            } catch (e) {
                // TODO: add logging
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                });
            }
        } else {
            res.status(500).send({
                status: 'error',
                message: 'Предоставлены некорректные данные'
            });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            }));
            await bot.send(...Array(1), JSON.stringify({
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'get',
                data: req.query
            }));
        }
    })();
});

function __setCallbackTimeout(callback, context, callbackArgs) {
    return new Promise(async (resolve, reject) => {
        try {
            setTimeout(() => {
                reject(new Error('timeout error'));
            }, 10000);
            resolve(await callback.call(context, ...callbackArgs));
        } catch (e) {
            reject(e);
        }
    });
}

function __processResponse(response) {
    const processed = [];
    const keys = [];
    response['metaData'].map((field) => {
        keys.push(field.name);
    });
    let objectData = null;
    response['rows'].map((row, index, arr) => {
        objectData = {};
        row.map((value, index) => {
            objectData[keys[index]] = value;
        });
        processed.push(objectData);
    });
    return processed;
}

function __log(message, level = 'error') {
    Logger.log([ 'error' ], level, message);
}

module.exports = router;