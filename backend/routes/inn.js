var express = require('express');
var router = express.Router();
const OracleDbConnector = require('../src/global/oracleDbConnection');
const ConfigProvider = require('../src/global/providers/ConfigProvider');
// const Logger = require('../src/global/winston-log-wrapper');
const { DealManager } = require('../src/megaplan_api/index');
const ClientV3 = require('../src/megaplan_api/megaplan-client-v3');

/* GET home page. */
router.get('/', function (req, res, next) {
    (async function() {
        const inn = req.query.value || null;
        if (inn) {
            try {
                const oracleDbConnector = new OracleDbConnector(ConfigProvider.get('oracle_cnd/connect'));
                const result = await oracleDbConnector.getInn(inn);
                res.send(result);
            } catch (e) {
                // TODO: add logging
                res.status(500).send({
                    status: 'error',
                    message: e.message,
                });
            }
        } else {
            res.send([]);
        }
    })();
});

router.post('/', function (req, res, next) {
    (async function() {
        const dealId = req.query.deal_id || null;
        const fieldValues = req.body;

        const client = new ClientV3('https://ssr.mos.ru', 'admin', 'Admin1Admin');
        const dealManagement = new DealManager(client);
        const deal = {
            'id': dealId,
            // ИНН заявителя
            'Category1000122CustomFieldInformatsiyaOZayaviteleInnZayavitelya': fieldValues[0],
            // ОГРН/ОГРНИП
            'Category1000122CustomFieldInformatsiyaOZayaviteleOgrn': fieldValues[1],
            // Наименование заявителя
            'Category1000122CustomFieldInformatsiyaOZayaviteleNaimenovanie': fieldValues[2],
            // Юридический адрес
            'Category1000122CustomFieldPervayaKassatsiyaFamiliya': fieldValues[3],
            // Тип заявителя
            'Category1000122CustomFieldInformatsiyaOZayaviteleTipZayavitelya': fieldValues[4],
            // Фамилия
            // 'Category1000122CustomFieldInformatsiyaOZayaviteleFamiliya': fieldValues[0],
            // Имя
            // 'Category1000122CustomFieldInformatsiyaOZayaviteleImya': fieldValues[0],
            // Отчество
            // 'Category1000122CustomFieldInformatsiyaOPredstaviteleZayavitely': fieldValues[0],
        };
        let response;
        try {
            response = await dealManagement.update(deal);
        } catch (e) {
            // TODO: add logging
            res.status(500).send({
                status: 'error',
                message: e.message,
            });
        }

        if (!response) {
            // TODO: add logging
            res.status(500).send({
                status: 'error',
                message: 'empty megaplan response',
            });
        }

        if (response.status === 200) {
            res.send({
                status: 'ok',
                data: response.data.data,
            });
        } else {
            // TODO: add logging
            res.status(response.status).send({
                status: 'error',
                message: response.meta.errors,
            });
        }
    })();
});

module.exports = router;
