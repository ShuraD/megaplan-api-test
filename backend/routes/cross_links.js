const express = require('express');
const router = express.Router();
const db = require('../src/global/models/index');
const Op = require('sequelize').Op;
const Logger = require('../src/global/winston-log-wrapper');
const TelegramBot = require('../src/telegram_bot/index');

const nioDbObjectModelName = 'NioObject';
const taxDbObjectModelName = 'TaxDisputesObject';
const bot = new TelegramBot();

router.post('/', function(req, res, next) {
    (async function() {
        const cadastralNumbers = req.body.cadastralNumbers;
        const excludedId = req.body.dealId;
        const type = req.body.type;
        // TODO: validate fields
        if (cadastralNumbers && cadastralNumbers.length && excludedId && type) {
            const modelTaxDisputes = db[taxDbObjectModelName];
            const modelNio = db[nioDbObjectModelName];
            const taxObjects = {};
            const nioObjects = {};
            try {
                for (let i = 0, counter = cadastralNumbers.length; i < counter; i++) {
                    switch (type) {
                        case 'tax_disputes':
                            taxObjects[cadastralNumbers[i]] = await modelTaxDisputes.findAll({
                                attributes: ['dealId'],
                                group: ['dealId'],
                                where: {
                                    dealId: {
                                        [Op.notIn]: [excludedId]
                                    },
                                    cadastralNumber: cadastralNumbers[i],
                                    isDeleted: false
                                }})
                                .map(item => item.dealId);
                            nioObjects[cadastralNumbers[i]] = await modelNio.findAll({
                                attributes: ['dealId'],
                                group: ['dealId'],
                                where: {
                                    cadastralNumber: cadastralNumbers[i],
                                    isDeleted: false
                                }})
                                .map(item => item.dealId);
                            break;
                        case 'nio':
                            taxObjects[cadastralNumbers[i]] = await modelTaxDisputes.findAll({
                                attributes: ['dealId'],
                                group: ['dealId'],
                                where: {
                                    cadastralNumber: cadastralNumbers[i],
                                    isDeleted: false
                                }})
                                .map(item => item.dealId);
                            nioObjects[cadastralNumbers[i]] = await modelNio.findAll({
                                attributes: ['dealId'],
                                group: ['dealId'],
                                where: {
                                    dealId: {
                                        [Op.notIn]: [excludedId]
                                    },
                                    cadastralNumber: cadastralNumbers[i],
                                    isDeleted: false
                                }})
                                .map(item => item.dealId);
                            break;
                    }
                }
            } catch (e) {
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body,
                    stack: e.stack
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body
                });
            }
            const result = {
                73: taxObjects,
                74: nioObjects
            };
            res.status(200).send({
                status: 'ok',
                data: result
            });
        } else {
            res.status(500).send({
                status: 'error',
                message: 'Предоставлены некорректные данные'
            });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'post',
                data: req.body
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: Налоговые споры (БП-73) / Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'post',
                data: req.body
            });
        }
    })();
});

function __log(message, level = 'error') {
    Logger.log([ 'error' ], level, message);
}

module.exports = router;