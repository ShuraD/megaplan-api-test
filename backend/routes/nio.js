const express = require('express');
const router = express.Router();
const db = require('../src/global/models/index');
const Logger = require('../src/global/winston-log-wrapper');
const TelegramBot = require('../src/telegram_bot/index');
const { DealManager } = require('../src/megaplan_api/index');
const ClientV3 = require('../src/megaplan_api/megaplan-client-v3');

// const validator = require('validator');

const dbObjectModelName = 'NioObject';
const dbObjectHistoryModelName = 'NioObjectsHistory';
const bot = new TelegramBot();

router.get('/object', function(req, res, next) {
    (async function() {
        try {
            const filter = JSON.parse(req.query.filter);
            const model = db[dbObjectModelName];
            const result = await model.findAll({
                where: {
                    dealId: filter.dealId,
                    isDeleted: false
                },
                order: [
                    ['id', 'ASC'],
                ]
            });
            res.status(200).send({
                status: 'ok',
                data: result
            });
        } catch (e) {
            res.status(500).send({
                status: 'error',
                message: e.message
            });
            __log(JSON.stringify({
                message: e.message,
                url: req.url,
                httpMethod: 'get',
                data: req.query
            }));
            try {
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: НИО(БП-74)',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.query
                });
            } catch (e) {
                __logTgBot(e.message);
            }
        }
    })();
});

router.post('/object/save', function(req, res, next) {
    (async function() {
        const data = req.body;
        // TODO: validate / sanitize fields
        const validationStatus = true;
        const validationResult = [];
        const validatedFields = {};
        const fields = [];
        // Fields
        // dealId
        // requirement
        // cadastralNumber
        // cadastralNumberParent
        // fullAddress
        // square
        // objectPurpose
        // objectType
        // cadastralValue
        // taxPotential
        // listInclusion
        // listItem
        // listYear
        // criteriaInclusion
        // disputeCategory
        // firstInstanceResult
        // appealInformation
        // appealCassationResult
        // cadastralNumberZU
        // vriZU
        // actNumber
        // actDate
        // copyrightHolder
        // cassationResult
        // objectLink
        // isDeleted
        // createdAt
        // updatedAt
        const fieldsBeforeUpdate = {}, fieldsAfterUpdate = {};
        if (validationStatus) {
            try {
                const model = db[dbObjectModelName];
                const history = db[dbObjectHistoryModelName];
                let object = await model.findByPk(data.id);
                if (object) {
                    // update object
                    for (let prop in data) {
                        if (object[prop] !== data[prop] && !['createdAt', 'updatedAt', 'mode', 'userId'].includes(prop)) {
                            fieldsBeforeUpdate[prop] = object[prop];
                            fieldsAfterUpdate[prop] = data[prop];
                        }
                        object[prop] = data[prop];
                    }
                    await object.save();
                    await history.create({
                        crud: 'UPDATE',
                        table: dbObjectModelName,
                        rowId: object.id,
                        userId: data.userId,
                        from:  fieldsBeforeUpdate,
                        to: fieldsAfterUpdate,
                    });
                } else {
                    // create object
                    delete data.id;
                    object = await model.create(data);
                    await history.create({
                        crud: 'CREATE',
                        table: dbObjectModelName,
                        rowId: object.id,
                        userId: data.userId,
                        from:  object.dataValues,
                    });
                }
                updateDeal(data.dealId);
                res.status(200).send({
                    status: 'ok',
                    data: object
                });
            } catch (e) {
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body
                });
            }
        } else {
            res.status(500).send({ status: 'error', message: 'Предоставлены некорректные данные' });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'post',
                data: req.body,
                result: validationResult
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'post',
                data: req.body,
                result: validationResult
            });
        }
    })();
});

router.post('/object/delete', function(req, res, next) {
    (async function() {
        const id = req.body.id;
        const userId = req.body.userId;
        // TODO: validate fields
        if (id) {
            try {
                const model = db[dbObjectModelName];
                const history = db[dbObjectHistoryModelName];
                const object = await model.findByPk(id);
                if (object) {
                    object.isDeleted = true;
                    await object.save();
                    // TODO: failed to create history for some reason?
                    await history.create({
                        crud: 'DELETE',
                        table: dbObjectModelName,
                        rowId: object.id,
                        userId: userId,
                        from:  object.dataValues,
                    });
                    updateDeal(object.dealId);
                    res.status(200).send({
                        status: 'ok'
                    });
                } else {
                    res.status(500).send({
                        status: 'error',
                        message: 'Запись отсутствует в БД'
                    });
                    __log(JSON.stringify({
                        message: 'Запись отсутствует в БД',
                        url: req.url,
                        httpMethod: 'post',
                        data: req.body
                    }));
                    await bot.send(...Array(1), {
                        message: 'Запись отсутствует в БД',
                        level: 'warn',
                        module: 'Перечень объектов: НИО (БП-74)',
                        url: req.url,
                        httpMethod: 'post',
                        data: req.body
                    });
                }
            } catch (e) {
                res.status(500).send({
                    status: 'error',
                    message: e.message
                });
                __log(JSON.stringify({
                    message: e.message,
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body
                }));
                await bot.send(...Array(1), {
                    message: e.message,
                    level: 'error',
                    module: 'Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'post',
                    data: req.body
                });
            }
        } else {
            res.status(500).send({
                status: 'error',
                message: 'Предоставлены некорректные данные'
            });
            __log(JSON.stringify({
                message: 'Предоставлены некорректные данные',
                url: req.url,
                httpMethod: 'post',
                data: req.body
            }));
            await bot.send(...Array(1), {
                message: 'Предоставлены некорректные данные',
                level: 'error',
                module: 'Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'post',
                data: req.body
            });
        }
    })();
});

router.get('/object/:id/history', function(req, res, next) {
    (async function() {
        const id = req.params.id;
        // TODO: validate id?
        try {
            const history = db[dbObjectHistoryModelName] || null;
            const items = await history.findAll({
                where: { rowId: id },
                order: [[ 'id', 'ASC' ]]
            });
            if (items && items.length > 0) {
                res.status(200).send({
                    status: 'ok',
                    data: items
                });
            } else {
                res.status(500).send({
                    status: 'error',
                    message: 'Запись отсутствует в БД'
                });
                __log(JSON.stringify({
                    message: 'Запись отсутствует в БД',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.params
                }));
                await bot.send(...Array(1), {
                    message: 'Запись отсутствует в БД',
                    level: 'error',
                    module: 'Перечень объектов: НИО (БП-74)',
                    url: req.url,
                    httpMethod: 'get',
                    data: req.params
                });
            }
        } catch (e) {
            res.status(500).send({
                status: 'error',
                message: e.message
            });
            __log(JSON.stringify({
                message: e.message,
                url: req.url,
                httpMethod: 'get',
                data: req.params
            }), 'error');
            await bot.send(...Array(1), {
                message: e.message,
                level: 'error',
                module: 'Перечень объектов: НИО (БП-74)',
                url: req.url,
                httpMethod: 'get',
                data: req.params
            });
        }
    })();
});

function __log(message, level = 'error') {
    Logger.log([ 'error' ], level, message);
}

function __logTgBot(message, level = 'error') {
    Logger.log([ 'bot' ], level, message);
}

function updateDeal(dealId) {
    (async function() {
        try {
            const model = db[dbObjectModelName];
            const objects = await model.findAll({
                where: {
                    dealId: dealId,
                    isDeleted: false
                }
            });
            const cadNums = [];
            const addresses = [];
            const dispCategories = [];
            const reqs = [];
            objects.forEach(obj => {
                if (!cadNums.includes(obj.cadastralNumber)) {
                    cadNums.push(obj.cadastralNumber);
                    // TODO: find address
                    const sorted = objects.filter(o => o.cadastralNumber === obj.cadastralNumber).sort((a, b) => {
                        if (!a.listYear && !b.listYear) return 0;
                        if (!a.listYear) return -1;
                        if (!b.listYear) return 1;
                        if (a.listYear > b.listYear) {
                            return 1;
                        } else if (a.listYear < b.listYear) {
                            return - 1;
                        } else {
                            return 0;
                        }
                    });
                    addresses.push(sorted[0].fullAddress);
                }
                if (obj.disputeCategory) {
                    const categories = obj.disputeCategory.split(';');
                    categories.forEach(c => {
                        if (!dispCategories.includes(c)) {
                            dispCategories.push(c);
                        }
                    });
                }
                if (obj.requirement && !reqs.includes(obj.requirement)) {
                    reqs.push(obj.requirement);
                }
            });

            const client = new ClientV3('https://ssr.mos.ru', 'admin', 'Admin1Admin');
            const dealManagement = new DealManager(client);
            const deal = {
                'id': dealId,
                // Информация об объекте: Кадастровый номер
                'Category1000122CustomFieldPervayaKassatsiyaDobavitDokumenti': cadNums.join('; '),
                // Информация об объекте: Адрес объекта
                'Category1000122CustomFieldPervayaKassatsiyaPrinyatoVPolzu': addresses.join('; '),
                // Информация об объекте: Категория спора
                'Category1000122CustomFieldDobavlenieNovogoObEktaVKartochkuDelo3': dispCategories.join('; '),
                // Информация об объекте: Требование (суть)
                'Category1000122CustomFieldStatusDela': reqs.join('; '),
            };
            // TODO: log error result
            return await dealManagement.update(deal);
        } catch (e) {
            // TODO: log error
            return e;
        }
    })();
}

module.exports = router;