const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
// const bodyParser = require('body-parser');

const indexRouter = require('./routes/index');
const apiTestRouter = require('./routes/api-test');
const taxDisputesRouter = require('./routes/tax_disputes');
const nioRouter = require('./routes/nio');
const dataMartRouter = require('./routes/data_mart');
const requestInfoRouter = require('./routes/request_info');
const crossLinksRouter = require('./routes/cross_links');
const innRouter = require('./routes/inn');

const app = express();
app.use(cors());

process.env.MODE = 'development';

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/megaplan/api', apiTestRouter);
app.use('/tax_disputes', taxDisputesRouter);
app.use('/nio', nioRouter);
app.use('/data_mart', dataMartRouter);
app.use('/request_info', requestInfoRouter);
app.use('/cross_links', crossLinksRouter);
app.use('/inn', innRouter);

// app.all('*', function(req, res, next) {
//     res.set('Access-Control-Allow-Origin', '*');
//     next();
// });

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', '*');
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
