import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: process.env.NODE_ENV === 'production'
            ? 'https://cnd-api.control-mos.ru'
            : 'https://cnd-dev.ddev.cnd'
    })
}
