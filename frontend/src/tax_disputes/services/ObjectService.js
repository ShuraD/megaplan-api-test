import api from './api';

export default {
    fetchObjects (id) {
        return api().get(`tax-disputes/deal/${id}/objects`);
    },
}
