import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Content from '../components/Content.vue';
import MegaplanPrograms from '../components/MegaplanPrograms.vue';
import MegaplanReportExcelConstructor from '../components/MegaplanReportExcelConstructor.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
        children: [
            {
                path: 'megaplan/programs',
                name: 'MegaplanPrograms',
                component: MegaplanPrograms,
            },
            {
                path: 'megaplan/report-constructor/excel',
                name: 'MegaplanReportExcelConstructor',
                component: MegaplanReportExcelConstructor,
            }
        ]
    },
    // {
    //     path: '/about',
    //     name: 'About',
    //     // route level code-splitting
    //     // this generates a separate chunk (about.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: function () {
    //         return import(/* webpackChunkName: "about" */ '../views/About.vue')
    //     }
    // }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router;
