import axios from 'axios';

class Api {
    constructor() {
        this.service = axios.create({
            baseURL: 'http://localhost:3000/megaplan/api',
        });
    }
    get(url) {
        let self = this;
        return new Promise(async (resolve, reject) => {
            try {
                let response = await self.service.get(url);
                if (response.status === 200) {
                    resolve({
                        status: 'ok',
                        data: response.data,
                    });
                } else {
                    resolve({
                        status: 'error',
                        reason: response.status,
                    })
                    // TODO: response error?
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    post(url, data = {}) {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await self.service.post(url, data);
                if (response.status === 200) {
                    resolve({
                        status: 'ok',
                        data: response.data,
                    });
                } else {
                    resolve({
                        status: 'error',
                        reason: response.status,
                    })
                    // TODO: response error?
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    getPrograms() {
        let self = this;
        const url = 'programs';
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await self.get(url));
            } catch (e) {
                reject(e);
            }
        });
    }
    getProgram(id) {
        let self = this;
        const url = 'program/' + id;
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await self.get(url));
            } catch (e) {
                reject(e);
            }
        })
    }
}

export default new Api();