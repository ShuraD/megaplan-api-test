import axios from 'axios';
import ConfigProvider from '../../global/providers/ConfigProvider';

class Api {
    constructor() {
        axios.defaults.baseURL = ConfigProvider.get('base_url') + '/grant-lease-holder';
    }
    get(url) {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await axios.get(url);
                if (response.status === 200) {
                    resolve({
                        status: 'ok',
                        data: response.data,
                    });
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    post(url, data = {}) {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await axios.post(url, data);
                if (response.status === 200) {
                    resolve({
                        status: 'ok',
                        data: response.data,
                    });
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    put(url, data = {}) {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await axios.put(url, data);
                if (response.status === 200) {
                    resolve({
                        status: 'ok',
                        data: response.data,
                    });
                }
            } catch (e) {
                reject(e);
            }
        });
    }
    getList() {
        let self = this;
        const url = '';
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await self.get(url));
            } catch (e) {
                reject(e);
            }
        });
    }
    create(data) {
        let self = this;
        const url = '';
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await self.post(url, data));
            } catch (e) {
                reject(e);
            }
        });
    }
    update(data) {
        let self = this;
        const url = '';
        return new Promise(async (resolve, reject) => {
            try {
                resolve(await self.put(url, data));
            } catch (e) {
                reject(e);
            }
        });
    }
}

export default new Api();