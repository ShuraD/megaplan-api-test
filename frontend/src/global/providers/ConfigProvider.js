// const CONFIG_DEV = 'config.frontend.dev.json';
// const CONFIG_PROD = 'config.frontend.prod.json';

class ConfigProvider {
    constructor() {
        let devConfig = require('../../../../config.frontend.dev.json');
        let prodConfig = null;
        try {
            prodConfig = require('../../../../config.frontend.prod.json');
        } catch (e) {
            // TODO: skipped internally
        } finally {
            this.config = devConfig;
            if (prodConfig) {
                this.config = Object.assign({}, this.config, prodConfig);
            }
        }
    }
    get(path) {
        const mode = this.config.mode;
        const keys = path.split('/');
        let tree = this.config;
        let result;
        for (let i = 0, counter = keys.length; i < counter; i++) {
            result = tree[keys[i]];
            if (typeof result === 'undefined') {
                result = tree[mode][keys[i]];
                if (typeof result === 'undefined') {
                    return null;
                }
            }
            tree = result;
        }
        return result;
    }
}

module.exports = new ConfigProvider();