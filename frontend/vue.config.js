module.exports = {
    "pages": {
        "index": {
            "entry": "./src/index/main.js",
            "template": "./public/index.html",
            "filename": "index.html",
            "title": "index page",
            "chunks": [
                "chunk-vendors",
                "chunk-common",
                "index"
            ]
        },
        "tax_disputes": {
            "entry": "./src/tax_disputes/main.js",
            "template": "./public/tax_disputes.html",
            "filename": "tax_disputes.html",
            "title": "tax_disputes",
            "chunks": [
                "chunk-vendors",
                "chunk-common",
                "tax_disputes"
            ]
        },
    },
    "devServer": {
        "historyApiFallback": {
            verbose: true,
            "rewrites": [
                {
                    from: /\/tax_disputes/,
                    to: '/tax_disputes.html'
                },
            ]
        }
    },
    // "transpileDependencies": [
    //     "vuetify"
    // ]
}
