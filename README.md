# Project

## Backend (Express JS)

### Configuration

config.backend.dev.yaml

Overrides by config.backend.prod.yaml

### Deployment

```bash
cd backend/
yarn install
```

### Grant

Module Grant

```bash
cd backend/src/grant/orm/
# run migrations
npx sequelize-cli db:migrate
```

## Frontend (Vue JS)

### Configuration

config.frontend.dev.json

Overrides by config.frontend.prod.json

### Deployment

```bash
cd frontend/
yarn install
yarn build
```